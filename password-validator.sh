#!/bin/bash
password=$1
isValid=0

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

error () {
    echo -e "${RED}$1${NC}"
}

if test "${#password}" -lt 10; then
    error "Password lenght should be greater than or equal 10"
    isValid=1
fi

echo "$password" | grep -q [0-9]
if test $? -eq 1; then
    error "Password should include numbers"
    isValid=1
fi

echo "$password" | grep -q [A-Z]
if test $? -eq 1; then
    error "Password should include capital letters"
    isValid=1
fi

echo "$password" | grep -q [a-z]
if test $? -eq 1; then
    error "Password should small case letters"
    isValid=1
fi

if test $isValid -eq 0; then
    echo -e "${GREEN}Strong password!${NC}" 
fi

exit $isValid