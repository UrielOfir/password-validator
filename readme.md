#  Password validation script
The script checks whether the password is strong according to the following criteria:
1. The password is at least 10 characters long.
2. The password includes uppercase letters, lowercase letters and numbers.

## How to run the script:
Clone the repository and run the script from the root directory:
./password_validation.sh <password>